
import { http } from '../../utils/http'
export default{
     fetchCates ({ commit }) {
        http.get("/cate/getAllCates").then((resp) => {
                commit('saveCates', resp.data.cates); 
        })
     }
}