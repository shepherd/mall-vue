import { createApp } from 'vue'
import DKToast from 'vue-dk-toast';
import App from './App.vue'
import router from './router'
import store from './store'

const app = createApp(App);
app.use(DKToast);
app.use(store)
app.use(router)
app.mount('#app')