import CryptoJS from 'crypto-js'
export function  populateFormData(obj,keys) {
        const params = new URLSearchParams();
        if(keys){
            for (const prop of keys) {
                params.append(prop, obj[prop])
            }
        }else{
            for (const prop in obj) {
               
                params.append(prop, obj[prop])
            }
        }
     return params;
}

export function  populateArrayFormData(arr,keys) {
    const params = new URLSearchParams();
    for(let item of arr){
        if(keys){
            for (const prop of keys) {
                params.append(prop, item[prop])
            }
        }else{
            for (const prop in item) {
                params.append(prop, item[prop])
            }
        }
    }
    
 return params;
}


export function encrypt (message, key) {
    var keyHex = CryptoJS.enc.Utf8.parse(key);
     var encrypted = CryptoJS.DES.encrypt(message, keyHex, {
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7
    });
    return encrypted.toString();
}

export function decrypt (message, key) {
    var keyHex = CryptoJS.enc.Utf8.parse(key);
    var plaintext = CryptoJS.DES.decrypt(message, keyHex, {
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7
    })
    return plaintext.toString(CryptoJS.enc.Utf8)
}