import axios from "axios";
export const  SERVER_URL='http://localhost:8888'
axios.defaults.withCredentials = true
export const http = axios.create({
    baseURL: SERVER_URL,
    headers: {'X-Requested-With': 'XMLHttpRequest'}
  });