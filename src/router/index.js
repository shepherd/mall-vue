import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import CateView from '../views/CateView.vue'
import ProductDetailView from '../views/ProductDetailView'
import CheckoutView from '../views/CheckoutView';
import OrderList from '../views/OrderList'
import OrderDetail from '../views/OrderDetail'
import My from '../views/My'
import Personal from '../views/my/Personal'
import MyAddress from '../views/my/MyAddress'
import Search from '../views/Search'
import PayResult from '../views/PayResult'

import { globalEventEmitter } from '../common/common';

const routes = [{
        path: '/',
        name: 'Home',
        component: Home,
        meta: {
            title: "首页"
        }
    },
    {
        path: '/cate/:id',
        name: 'CateView',
        component: CateView,
        meta: {
            title: "商品分类"
        }
    },
    {
        path: '/product/:id',
        name: 'ProductDetail',
        component: ProductDetailView,
        meta: {
            title: "商品详情"
        }

    },
    {
        path: '/search/',
        name: 'Search',
        component: Search,
        meta: {
            title: "商品搜索"
        }
    },
    {
        path: '/checkout/:type',
        name: 'CheckOut',
        component: CheckoutView,
        meta: {
            isAuth: true,
            title: "生成订单"
        }
    },
    {
        path: '/myorder',
        name: 'OrderList',
        component: OrderList,
        meta: {
            isAuth: true,
            title: "我的订单"
        }
    },
    {
        path: '/orderdetail/:id',
        name: 'OrderDetail',
        component: OrderDetail,
        meta: {
            isAuth: true,
            title: "订单详情"
        }
    },
    {
        path: '/my',
        name: 'My',
        component: My,
        meta: {
            isAuth: true,
            title: "个人中心"
        },
        children: [{
                path: 'personal',
                name: 'Personal',
                component: Personal,
                meta: {
                    title: "管理个人信息"
                }
            },
            {
                path: 'address',
                name: 'MyAddress',
                component: MyAddress,
                meta: {
                    title: "管理收货地址"
                }
            }
        ]
    },
    {
        path: '/payresult',
        name: 'PayResult',
        component: PayResult,
        meta: {
            title: "订单支付"
        }
    }
];

const router = createRouter({
        history: createWebHistory(process.env.BASE_URL),
        routes,
        scrollBehavior() {
            // 始终滚动到顶部
            return { top: 0 }
        },
    })
    //前置路由守卫，验证是否登录
router.beforeEach(async(to, from, next) => {
        if (to.meta.isAuth) {
            const loguser = sessionStorage.getItem('user')
            if (loguser) {
                return next()
            } else {
                globalEventEmitter.emit('needLogin')
                return false;
            }
        } else {
            next()
        }
    })
    //后置路由守卫，修改页面标题
router.afterEach((to) => {
    document.title = '跨境电商服务平台 - ' + to.meta.title
})

export default router